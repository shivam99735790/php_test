<!-- This script will print customer ids of customer having logged-in 3 or more consecutive days based on log entries -->
<?php

/*-- Log Entries [Tab Delimited] String --*/
$log = '08-Jun-2012 1:00 AM 4ABCDEFGHI
11-Jun-2012 1:00 AM 1ABCDEFGHI
10-Jun-2012 1:00 AM 1ABCDEFGHI
09-Jun-2012 2:00 AM 1ABCDEFGHI
11-Jun-2012 8:23 AM 3ABCDEFGHI';

$log_array = explode(PHP_EOL, $log);   //Creating Array of Logs Line

//Sorting Log Entries Array in ASC Order
sort($log_array);

$final_customer_ids = array();  //This will store final customer ids
$line_log_array = array();  //This will store arrays of line segments 

foreach ($log_array as $log_line) {
    //Exploding to Split User ID and Date
    $line_log_array[] = explode(" ", $log_line);
}


//Searching 3 or more consecutive log dates for any customer ids -- [ASC Order Search Algo]
foreach ($line_log_array as $t_line) {
    $log_date_search = $t_line[0];  //Store Search Date
    $log_id_search = $t_line[3];    //Store Search Customer Id

    $consecutive_days_counter = 0;  //Counting for consecutive log entries date
    $start_date = null;
    $next_date = null;
    //Search consecutive 3 or more days through all log lines
    foreach ($line_log_array as $line) {
        $log_date = $line[0];
        $log_id = $line[3];

        if ($log_id_search == $log_id) {

            if ($consecutive_days_counter == 0) {
                $check_date = $log_date;

                /*-- Checking and generating new next consecutive date[This will help in handling month/year change] --*/
                $t_date = new DateTime($check_date);
                date_add($t_date, date_interval_create_from_date_string("1 day"));
                $next_date = date_format($t_date, 'd-M-Y');

                $consecutive_days_counter++;
            } else {

                if ($log_date == $next_date) {

                    /*-- Checking and generating new next consecutive date[This will help in handling month/year change] --*/
                    $t_date = new DateTime($next_date);
                    date_add($t_date, date_interval_create_from_date_string("1 day"));
                    $next_date = date_format($t_date, 'd-M-Y');

                    $consecutive_days_counter++;
                }
            }
        }
    }

    //Storing customers id only if there is 3 or more consecutive days log entries
    if ($consecutive_days_counter >= 3) {
        $final_customer_ids[] = $log_id_search;
    }
}

//Taking out unique customer ids
$final_customer_ids = array_unique($final_customer_ids);

//Printing Result
echo "Final Result[Customer Ids having 3 or more consecutive log entries]: <br />";
foreach ($final_customer_ids as $cid) {
    echo "<br />" . $cid;
}
